package com.hw.employee_control_system.connection;

import lombok.extern.java.Log;

import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

@Log
public class PGConnection {

    private final static String URL = parsePropertyFor("db.url");
    private final static String USER = parsePropertyFor("db.user");
    private final static String PASSWORD = parsePropertyFor("db.password");

    private PGConnection() {

    }

    public static Connection getConnection() throws SQLException {
        return DriverManager.getConnection(URL, USER, PASSWORD);
    }

    private static String parsePropertyFor(String propertyName){
        try (InputStream input = PGConnection.class.getClassLoader().getResourceAsStream("application.properties")) {
            Properties prop = new Properties();
            System.out.println();
            prop.load(input);
            return prop.getProperty(propertyName);
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        return "";
    }

}