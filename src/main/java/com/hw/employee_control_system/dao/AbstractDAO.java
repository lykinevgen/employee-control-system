package com.hw.employee_control_system.dao;

import com.hw.employee_control_system.connection.PGConnection;
import lombok.extern.java.Log;

import java.sql.*;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

@Log
public abstract class AbstractDAO<E, K> implements DAO<E, K> {

    abstract protected String getInsert();
    abstract protected String getSelectById();
    abstract protected String getSelect();
    abstract protected String getUpdateById();
    abstract protected String getDeleteById();

    @Override
    public long create(E entity) throws SQLException {
        try( Connection connection = PGConnection.getConnection()) {
            PreparedStatement preparedStatement = connection.prepareStatement(getInsert(), Statement.RETURN_GENERATED_KEYS);
            setPlaceholdersInStatementForCreate(preparedStatement, entity);
            preparedStatement.executeUpdate();
            try (ResultSet keys = preparedStatement.getGeneratedKeys()) {
                keys.next();
                log.info(preparedStatement.toString() + keys.getLong(1));
                return keys.getLong(1);
            }
        }
    }

    @Override
    public Optional<E> read(K id) throws SQLException {
        try(Connection connection = PGConnection.getConnection()){
            PreparedStatement preparedStatement = connection.prepareStatement(getSelectById());
            setPlaceholdersInStatementForRead(preparedStatement, id);
            ResultSet resultSet = preparedStatement.executeQuery();
            log.info(preparedStatement.toString());
            E entity = null;
            while(resultSet.next()) entity = getEntityFromResultSet(resultSet);
            return Optional.ofNullable(entity);
        }
    }

    @Override
    public Optional<List<E>> readAll() throws SQLException {
        try(Connection connection = PGConnection.getConnection()){
            PreparedStatement preparedStatement = connection.prepareStatement(getSelect());
            ResultSet resultSet = preparedStatement.executeQuery();
            log.info(preparedStatement.toString());
            LinkedList<E> list = new LinkedList<>();
            while(resultSet.next()) list.add(getEntityFromResultSet(resultSet));
            return list.isEmpty() ? Optional.empty() : Optional.of(list);
        }
    }

    @Override
    public boolean update(E entity) throws SQLException {
        try(Connection connection = PGConnection.getConnection()){
            PreparedStatement preparedStatement = connection.prepareStatement(getUpdateById());
            setPlaceholdersInStatementForUpdate(preparedStatement, entity);
            int result = preparedStatement.executeUpdate();
            log.info(preparedStatement.toString() +"\nRETURNING "+ result);
            return  result > 0;
        }
    }

    @Override
    public boolean delete(K id) throws SQLException {
        try(Connection connection = PGConnection.getConnection()){
            PreparedStatement preparedStatement = connection.prepareStatement(getDeleteById());
            setPlaceholdersInStatementForDelete(preparedStatement, id);
            int result = preparedStatement.executeUpdate();
            log.info(preparedStatement.toString() +"\nRETURNING "+ result);
            return  result > 0;
        }
    }

    protected abstract void setPlaceholdersInStatementForCreate(PreparedStatement preparedStatement, E entity) throws SQLException;

    protected abstract void setPlaceholdersInStatementForRead(PreparedStatement preparedStatement, K id) throws SQLException;

    protected abstract void setPlaceholdersInStatementForUpdate(PreparedStatement preparedStatement, E entity) throws SQLException;

    protected abstract void setPlaceholdersInStatementForDelete(PreparedStatement preparedStatement, K id) throws SQLException;

    protected abstract E getEntityFromResultSet(ResultSet resultSet) throws SQLException;
}
