package com.hw.employee_control_system.dao;

import java.sql.SQLException;
import java.util.List;
import java.util.Optional;


public interface DAO <E, K>{
    long create(E entity) throws SQLException;
    Optional<E> read(K id) throws SQLException;
    Optional<List<E>> readAll() throws SQLException;
    boolean update(E entity) throws SQLException;
    boolean delete(K id) throws SQLException;
}
