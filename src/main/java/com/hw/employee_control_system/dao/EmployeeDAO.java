package com.hw.employee_control_system.dao;

import com.hw.employee_control_system.entity.EmployeeEntity;
import com.hw.employee_control_system.enums.EnglishLanguageLevel;
import com.hw.employee_control_system.enums.Seniority;
import lombok.AccessLevel;
import lombok.experimental.FieldDefaults;
import lombok.extern.java.Log;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;

@Log
@FieldDefaults(makeFinal=true, level= AccessLevel.PRIVATE)
public class EmployeeDAO extends AbstractDAO<EmployeeEntity, Long> {

    private static final String INSERT = "INSERT INTO employee (first_name, second_name, third_name, date_of_birth, date_of_employment, " +
            "seniority, english_language_level, email, phone_number, skype, project_id, feedback_id) " +
            "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) RETURNING id";
    private static final String SELECT_BY_ID = "SELECT * FROM employee WHERE id = ?";
    private static final String SELECT = "SELECT * FROM employee";
    private static final String UPDATE_BY_ID = "UPDATE employee " +
            "SET first_name = ?, second_name = ?, third_name = ?, date_of_birth = ?, date_of_employment = ?, " +
            "seniority = ?, english_language_level = ?, email = ?, phone_number = ?, skype = ?, " +
            "project_id = ?, feedback_id = ?" +
            "WHERE id = ?";
    private static final String DELETE_BY_ID = "DELETE FROM employee WHERE id = ?";

    @Override
    protected String getInsert() {
        return INSERT;
    }

    @Override
    protected String getSelectById() {
        return SELECT_BY_ID;
    }

    @Override
    protected String getSelect() {
        return SELECT;
    }

    @Override
    protected String getUpdateById() {
        return UPDATE_BY_ID;
    }

    @Override
    protected String getDeleteById() {
        return DELETE_BY_ID;
    }

    @Override
    protected void setPlaceholdersInStatementForCreate(PreparedStatement preparedStatement, EmployeeEntity entity) throws SQLException {
        setFieldsInStatement(preparedStatement, entity);
    }

    @Override
    protected void setPlaceholdersInStatementForRead(PreparedStatement preparedStatement, Long id) throws SQLException {
        preparedStatement.setObject(1, id);
    }

    @Override
    protected void setPlaceholdersInStatementForUpdate(PreparedStatement preparedStatement, EmployeeEntity entity) throws SQLException {
        setFieldsInStatement(preparedStatement, entity);
        preparedStatement.setObject(13, entity.getId());
    }

    @Override
    protected void setPlaceholdersInStatementForDelete(PreparedStatement preparedStatement, Long id) throws SQLException {
        preparedStatement.setObject(1, id);
    }

    @Override
    protected EmployeeEntity getEntityFromResultSet(ResultSet resultSet) throws SQLException {
        return EmployeeEntity.builder()
                .id(resultSet.getObject("id", Long.class))
                .firstName(resultSet.getString("first_name"))
                .secondName(resultSet.getString("second_name"))
                .thirdName(resultSet.getString("third_name"))
                .dateOfBirth(resultSet.getDate("date_of_birth"))
                .dateOfEmployment(resultSet.getDate("date_of_employment"))
                .seniority(Seniority.valueOf(resultSet.getString("seniority")))
                .englishLanguageLevel(EnglishLanguageLevel.valueOf(resultSet.getString("english_language_level")))
                .email(resultSet.getString("email"))
                .phoneNumber(resultSet.getString("phone_number"))
                .skype(resultSet.getString("skype"))
                .project_id(resultSet.getObject("project_id", Long.class))
                .feedback_id(resultSet.getObject("feedback_id", Long.class)).build();
    }

    private void setFieldsInStatement(PreparedStatement preparedStatement, EmployeeEntity entity) throws SQLException {
        preparedStatement.setString(1, entity.getFirstName());
        preparedStatement.setString(2, entity.getSecondName());
        preparedStatement.setString(3, entity.getThirdName());
        preparedStatement.setDate(4, entity.getDateOfBirth());
        preparedStatement.setDate(5, entity.getDateOfEmployment());
        preparedStatement.setString(6, entity.getSeniority().name());
        preparedStatement.setString(7, entity.getEnglishLanguageLevel().name());
        preparedStatement.setString(8, entity.getEmail());
        preparedStatement.setString(9, entity.getPhoneNumber());
        preparedStatement.setString(10, entity.getSkype());
        preparedStatement.setObject(11, entity.getProject_id(), Types.BIGINT);
        preparedStatement.setObject(12, entity.getFeedback_id(), Types.BIGINT);
    }
}
