package com.hw.employee_control_system.dao;

import com.hw.employee_control_system.entity.FeedbackEntity;
import lombok.extern.java.Log;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

@Log
public class FeedbackDAO extends AbstractDAO<FeedbackEntity, Long> {


    private static final String INSERT = "INSERT INTO feedback (description, date) " +
            "VALUES (?, ?)";
    private static final String SELECT_BY_ID = "SELECT * FROM feedback WHERE id = ?";
    private static final String SELECT = "SELECT * FROM feedback";
    private static final String UPDATE_BY_ID = "UPDATE feedback " +
            "SET description = ?, date = ?" +
            "WHERE id = ?";
    private static final String DELETE_BY_ID = "DELETE FROM feedback WHERE id = ?";

    @Override
    protected String getInsert() {
        return INSERT;
    }

    @Override
    protected String getSelectById() {
        return SELECT_BY_ID;
    }

    @Override
    protected String getSelect() {
        return SELECT;
    }

    @Override
    protected String getUpdateById() {
        return UPDATE_BY_ID;
    }

    @Override
    protected String getDeleteById() {
        return DELETE_BY_ID;
    }

    @Override
    protected void setPlaceholdersInStatementForCreate(PreparedStatement preparedStatement, FeedbackEntity entity) throws SQLException {
        setFieldsInStatement(preparedStatement, entity);
    }

    @Override
    protected void setPlaceholdersInStatementForRead(PreparedStatement preparedStatement, Long id) throws SQLException {
        preparedStatement.setObject(1, id);
    }

    @Override
    protected void setPlaceholdersInStatementForUpdate(PreparedStatement preparedStatement, FeedbackEntity entity) throws SQLException {
        setFieldsInStatement(preparedStatement, entity);
        preparedStatement.setObject(3, entity.getId());
    }

    @Override
    protected void setPlaceholdersInStatementForDelete(PreparedStatement preparedStatement, Long id) throws SQLException {
        preparedStatement.setObject(1, id);
    }

    protected FeedbackEntity getEntityFromResultSet(ResultSet resultSet) throws SQLException {
        return FeedbackEntity.builder()
                .id(resultSet.getLong("id"))
                .description(resultSet.getString("description"))
                .date(resultSet.getDate("date"))
                .build();
    }

    private void setFieldsInStatement(PreparedStatement preparedStatement, FeedbackEntity entity) throws SQLException {
        preparedStatement.setString(1, entity.getDescription());
        preparedStatement.setDate(2, entity.getDate());
    }
}
