package com.hw.employee_control_system.dao;

import com.hw.employee_control_system.entity.ProjectEntity;
import com.hw.employee_control_system.enums.Methodology;
import lombok.extern.java.Log;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;

@Log
public class ProjectDAO extends AbstractDAO<ProjectEntity, Long> {



    private static final String INSERT = "INSERT INTO project (project_name, client, start_date, end_date, methodology, project_manager_id, team_id) " +
            "VALUES (?, ?, ?, ?, ?, ?, ?)";
    private static final String SELECT_BY_ID = "SELECT * FROM project WHERE id = ?";
    private static final String SELECT = "SELECT * FROM project";
    private static final String UPDATE_BY_ID = "UPDATE project " +
            "SET project_name = ?, client = ?, start_date = ?, end_date = ?, methodology = ?, project_manager_id = ?, team_id = ?" +
            "WHERE id = ?";
    private static final String DELETE_BY_ID = "DELETE FROM project WHERE id = ?";

    @Override
    protected String getInsert() {
        return INSERT;
    }

    @Override
    protected String getSelectById() {
        return SELECT_BY_ID;
    }

    @Override
    protected String getSelect() {
        return SELECT;
    }

    @Override
    protected String getUpdateById() {
        return UPDATE_BY_ID;
    }

    @Override
    protected String getDeleteById() {
        return DELETE_BY_ID;
    }

    @Override
    protected void setPlaceholdersInStatementForCreate(PreparedStatement preparedStatement, ProjectEntity entity) throws SQLException {
        setFieldsInStatement(preparedStatement, entity);
    }

    @Override
    protected void setPlaceholdersInStatementForRead(PreparedStatement preparedStatement, Long id) throws SQLException {
        preparedStatement.setObject(1, id);
    }

    @Override
    protected void setPlaceholdersInStatementForUpdate(PreparedStatement preparedStatement, ProjectEntity entity) throws SQLException {
        setFieldsInStatement(preparedStatement, entity);
        preparedStatement.setObject(8, entity.getId());
    }

    @Override
    protected void setPlaceholdersInStatementForDelete(PreparedStatement preparedStatement, Long id) throws SQLException {
        preparedStatement.setObject(1, id);
    }

    @Override
    protected ProjectEntity getEntityFromResultSet(ResultSet resultSet) throws SQLException {
        return ProjectEntity.builder()
                .id(resultSet.getObject("id", Long.class))
                .projectName(resultSet.getString("project_name"))
                .client(resultSet.getString("client"))
                .start(resultSet.getDate("start_date"))
                .end(resultSet.getDate("end_date"))
                .methodology(Methodology.valueOf(resultSet.getString("methodology")))
                .projectManagerId(resultSet.getObject("project_manager_id", Long.class))
                .build();
    }

    private void setFieldsInStatement(PreparedStatement preparedStatement, ProjectEntity entity) throws SQLException {
        preparedStatement.setString(1, entity.getProjectName());
        preparedStatement.setString(2, entity.getClient());
        preparedStatement.setDate(3, entity.getStart());
        preparedStatement.setDate(4, entity.getEnd());
        preparedStatement.setString(5, entity.getMethodology().name());
        preparedStatement.setObject(6, entity.getProjectManagerId(), Types.BIGINT);
        preparedStatement.setObject(7, entity.getTeamId(), Types.BIGINT);
    }

}
