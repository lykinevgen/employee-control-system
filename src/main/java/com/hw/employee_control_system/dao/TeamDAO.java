package com.hw.employee_control_system.dao;

import com.hw.employee_control_system.entity.TeamEntity;
import lombok.extern.java.Log;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

@Log
public class TeamDAO extends AbstractDAO<TeamEntity, Long> {
    private static final String INSERT = "INSERT INTO team (team_name) VALUES (?)";
    private static final String SELECT_BY_ID = "SELECT * FROM team WHERE id = ?";
    private static final String SELECT = "SELECT * FROM team";
    private static final String UPDATE_BY_ID = "UPDATE team " +
            "SET team_name = ?" +
            "WHERE id = ?";
    private static final String DELETE_BY_ID = "DELETE FROM team WHERE id = ?";


    @Override
    protected String getInsert() {
        return INSERT;
    }

    @Override
    protected String getSelectById() {
        return SELECT_BY_ID;
    }

    @Override
    protected String getSelect() {
        return SELECT;
    }

    @Override
    protected String getUpdateById() {
        return UPDATE_BY_ID;
    }

    @Override
    protected String getDeleteById() {
        return DELETE_BY_ID;
    }

    @Override
    protected void setPlaceholdersInStatementForCreate(PreparedStatement preparedStatement, TeamEntity entity) throws SQLException {
        setFieldsInStatement(preparedStatement, entity);
    }

    @Override
    protected void setPlaceholdersInStatementForRead(PreparedStatement preparedStatement, Long id) throws SQLException {
        preparedStatement.setObject(1, id);
    }

    @Override
    protected void setPlaceholdersInStatementForUpdate(PreparedStatement preparedStatement, TeamEntity entity) throws SQLException {
        setFieldsInStatement(preparedStatement, entity);
        preparedStatement.setObject(2, entity.getId());
    }

    @Override
    protected void setPlaceholdersInStatementForDelete(PreparedStatement preparedStatement, Long id) throws SQLException {
        setPlaceholdersInStatementForRead(preparedStatement, id);
    }

    protected TeamEntity getEntityFromResultSet(ResultSet resultSet) throws SQLException {
        return TeamEntity.builder()
                .id(resultSet.getLong("id"))
                .teamName(resultSet.getString("team_name"))
                .build();
    }

    private void setFieldsInStatement(PreparedStatement preparedStatement, TeamEntity entity) throws SQLException {
        preparedStatement.setString(1, entity.getTeamName());
    }
}
