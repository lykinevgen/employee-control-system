package com.hw.employee_control_system.dao;

import com.hw.employee_control_system.connection.PGConnection;
import com.hw.employee_control_system.entity.TeamToEmployeeEntity;
import lombok.extern.java.Log;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

@Log
public class TeamToEmployeeDAO {


    private static final String INSERT = "INSERT INTO team_employee (team_id, employee_id) " +
            "VALUES (?, ?)";
    private static final String SELECT_BY_ID = "SELECT * FROM team_employee WHERE team_id = ? and employee_id = ?";
    private static final String SELECT = "SELECT * FROM team_employee";
    private static final String UPDATE_BY_ID = "UPDATE team_employee " +
            "SET team_id = ?, employee_id = ?" +
            "WHERE team_id = ? and employee_id = ?";
    private static final String DELETE_BY_ID = "DELETE FROM team_employee WHERE team_id = ? and employee_id = ?";

    public long create(TeamToEmployeeEntity entity) throws SQLException {
        try( Connection connection = PGConnection.getConnection()) {
            PreparedStatement preparedStatement = connection.prepareStatement(INSERT);
            setFieldsInStatement(preparedStatement, entity);

            int result = preparedStatement.executeUpdate();
            log.info(preparedStatement.toString() + result);
            return result;
        }
    }

    public Optional<TeamToEmployeeEntity> read(TeamToEmployeeEntity entity ) throws SQLException {
        try(Connection connection = PGConnection.getConnection()) {
            PreparedStatement preparedStatement = connection.prepareStatement(SELECT_BY_ID);
            setFieldsInStatement(preparedStatement, entity);
            ResultSet resultSet = preparedStatement.executeQuery();
            log.info(preparedStatement.toString());
            TeamToEmployeeEntity team = null;
            while(resultSet.next()){
                team = getEntityFromResultSet(resultSet);
            }
            return Optional.ofNullable(team);
        }
    }

    public Optional<List<TeamToEmployeeEntity>> readAll() throws SQLException {
        try(Connection connection = PGConnection.getConnection()) {
            PreparedStatement preparedStatement = connection.prepareStatement(SELECT);
            ResultSet resultSet = preparedStatement.executeQuery();
            log.info(preparedStatement.toString());
            LinkedList<TeamToEmployeeEntity> list = new LinkedList<>();
            while(resultSet.next()) list.add(getEntityFromResultSet(resultSet));
            return list.isEmpty()? Optional.empty() : Optional.of(list);
        }
    }

    public boolean update( TeamToEmployeeEntity oldEntity, TeamToEmployeeEntity newEntity ) throws SQLException {
        try( Connection connection = PGConnection.getConnection()) {
            PreparedStatement preparedStatement = connection.prepareStatement(UPDATE_BY_ID);
            setFieldsInStatement(preparedStatement, newEntity);
            preparedStatement.setLong(3, oldEntity.getTeamId());
            preparedStatement.setLong(4, oldEntity.getEmployeeId());
            int result = preparedStatement.executeUpdate();
            log.info(preparedStatement.toString() +"\nRETURNING "+ result);
            return  result > 0;
        }
    }

    public boolean delete(TeamToEmployeeEntity entityToDelete) throws SQLException {
        try(Connection connection = PGConnection.getConnection()){
            PreparedStatement preparedStatement = connection.prepareStatement(DELETE_BY_ID);
            setFieldsInStatement(preparedStatement, entityToDelete);
            int result = preparedStatement.executeUpdate();
            log.info(preparedStatement.toString() +"\nRETURNING "+ result);
            return  result > 0;
        }
    }

    private TeamToEmployeeEntity getEntityFromResultSet(ResultSet resultSet) throws SQLException {
        return TeamToEmployeeEntity.builder()
                .teamId(resultSet.getLong("team_id"))
                .employeeId(resultSet.getLong("employee_id"))
                .build();
    }

    private void setFieldsInStatement(PreparedStatement preparedStatement, TeamToEmployeeEntity entity) throws SQLException {
        preparedStatement.setLong(1, entity.getTeamId());
        preparedStatement.setLong(2, entity.getEmployeeId());
    }

}
