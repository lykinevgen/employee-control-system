package com.hw.employee_control_system.entity;

import com.hw.employee_control_system.enums.EnglishLanguageLevel;
import com.hw.employee_control_system.enums.Seniority;
import lombok.*;
import lombok.experimental.FieldDefaults;

import java.sql.Date;

@NoArgsConstructor
@AllArgsConstructor
@FieldDefaults(makeFinal=false, level= AccessLevel.PRIVATE)
@Getter
@Setter
@Builder
@EqualsAndHashCode
public class EmployeeEntity {
    Long id;
    String firstName;
    String secondName;
    String thirdName;
    Date dateOfBirth;
    Date dateOfEmployment;
    Seniority seniority;
    EnglishLanguageLevel englishLanguageLevel;
    String email;
    String phoneNumber;
    String skype;
    Long project_id;
    Long feedback_id;
}
