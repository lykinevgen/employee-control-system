package com.hw.employee_control_system.entity;

import lombok.*;
import lombok.experimental.FieldDefaults;
import java.sql.Date;

@NoArgsConstructor
@AllArgsConstructor
@FieldDefaults(makeFinal=false, level= AccessLevel.PRIVATE)
@Getter
@Setter
@Builder
@EqualsAndHashCode
public class FeedbackEntity {
    long id;
    String description;
    Date date;
}
