package com.hw.employee_control_system.entity;

import com.hw.employee_control_system.enums.Methodology;
import lombok.*;
import lombok.experimental.FieldDefaults;

import java.sql.Date;

@NoArgsConstructor
@AllArgsConstructor
@FieldDefaults(makeFinal=false, level= AccessLevel.PRIVATE)
@Getter
@Setter
@Builder
@EqualsAndHashCode
public class ProjectEntity {
    long id;
    String projectName;
    String client;
    Date start;
    Date end;
    Methodology methodology;
    Long projectManagerId;
    Long teamId;
}
