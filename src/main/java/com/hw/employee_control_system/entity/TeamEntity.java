package com.hw.employee_control_system.entity;

import lombok.*;
import lombok.experimental.FieldDefaults;

@NoArgsConstructor
@AllArgsConstructor
@FieldDefaults(makeFinal=false, level= AccessLevel.PRIVATE)
@Getter
@Setter
@Builder
@EqualsAndHashCode
public class TeamEntity {
    Long id;
    String teamName;
}
