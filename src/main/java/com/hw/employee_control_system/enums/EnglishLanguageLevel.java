package com.hw.employee_control_system.enums;

public enum EnglishLanguageLevel {
    A1, A2, B1, B2, C1, C2;
}
