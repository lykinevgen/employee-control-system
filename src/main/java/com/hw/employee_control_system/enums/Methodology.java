package com.hw.employee_control_system.enums;

public enum Methodology {
    AGILE, SCRUM, FDD, LEAN, EP, WATERFALL, PROTOTYPING, RAD, DSD, SPIRAL, JAD, RUP;
}
