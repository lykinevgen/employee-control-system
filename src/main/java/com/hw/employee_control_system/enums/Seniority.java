package com.hw.employee_control_system.enums;

public enum Seniority {
    J1, J2, M1, M2, S1, S2;
}
