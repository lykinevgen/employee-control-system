package com.hw.employee_control_system.connection;

import org.junit.jupiter.api.Test;

import java.sql.Connection;
import java.sql.SQLException;

import static org.junit.jupiter.api.Assertions.assertFalse;

public class PGConnectionTests {


    @Test
    public void getConnection() throws SQLException {
        assertFalse(PGConnection.getConnection().isClosed());
    }

    @Test
    public void getSeveralConnections() throws SQLException {
        Connection connection = PGConnection.getConnection();
        Connection connection2 = PGConnection.getConnection();
        Connection connection3 = PGConnection.getConnection();
        Connection connection4 = PGConnection.getConnection();
        Connection connection5 = PGConnection.getConnection();
        connection.close();
        connection2.close();
        connection3.close();
        connection4.close();
        connection5.close();
    }

}
