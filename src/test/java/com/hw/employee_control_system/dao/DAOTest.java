package com.hw.employee_control_system.dao;

import com.hw.employee_control_system.connection.PGConnection;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;

import java.sql.Connection;
import java.sql.SQLException;

public class DAOTest {

    @BeforeEach
    public void before() throws SQLException {

        try(Connection connection = PGConnection.getConnection()){
            connection.prepareStatement("TRUNCATE employee, team, project, feedback, team_employee CASCADE;").execute();
        }
    }

    @BeforeAll
    public static void beforeAll() throws SQLException {

        try(Connection connection = PGConnection.getConnection()){
            connection.prepareStatement("DROP SCHEMA public CASCADE;").execute();
            connection.prepareStatement("CREATE SCHEMA public;").execute();
            connection.prepareStatement("-- public.feedback definition\n" +
                    "\n" +
                    "-- Drop table\n" +
                    "\n" +
                    "-- DROP TABLE feedback;\n" +
                    "\n" +
                    "CREATE TABLE feedback (\n" +
                    "\tid int8 NOT NULL GENERATED ALWAYS AS IDENTITY,\n" +
                    "\tdescription text NULL,\n" +
                    "\t\"date\" date NULL,\n" +
                    "\tCONSTRAINT feedback_pk PRIMARY KEY (id)\n" +
                    ");\n" +
                    "\n" +
                    "\n" +
                    "-- public.team definition\n" +
                    "\n" +
                    "-- Drop table\n" +
                    "\n" +
                    "-- DROP TABLE team;\n" +
                    "\n" +
                    "CREATE TABLE team (\n" +
                    "\tid int8 NOT NULL GENERATED ALWAYS AS IDENTITY,\n" +
                    "\tteam_name varchar NOT NULL,\n" +
                    "\tCONSTRAINT team_pk PRIMARY KEY (id),\n" +
                    "\tCONSTRAINT team_un UNIQUE (team_name)\n" +
                    ");\n" +
                    "\n" +
                    "\n" +
                    "-- public.employee definition\n" +
                    "\n" +
                    "-- Drop table\n" +
                    "\n" +
                    "-- DROP TABLE employee;\n" +
                    "\n" +
                    "CREATE TABLE employee (\n" +
                    "\tid int8 NOT NULL GENERATED ALWAYS AS IDENTITY,\n" +
                    "\tfirst_name varchar NULL,\n" +
                    "\tsecond_name varchar NULL,\n" +
                    "\tthird_name varchar NULL,\n" +
                    "\tdate_of_birth date NULL,\n" +
                    "\tdate_of_employment date NULL,\n" +
                    "\tseniority varchar NULL,\n" +
                    "\tenglish_language_level varchar NULL,\n" +
                    "\temail varchar NULL,\n" +
                    "\tphone_number varchar NULL,\n" +
                    "\tskype varchar NULL,\n" +
                    "\tproject_id int8 NULL,\n" +
                    "\tfeedback_id int8 NULL,\n" +
                    "\tCONSTRAINT employee_pk PRIMARY KEY (id),\n" +
                    "\tCONSTRAINT employee_fk FOREIGN KEY (feedback_id) REFERENCES feedback(id)\n" +
                    ");\n" +
                    "\n" +
                    "\n" +
                    "-- public.project definition\n" +
                    "\n" +
                    "-- Drop table\n" +
                    "\n" +
                    "-- DROP TABLE project;\n" +
                    "\n" +
                    "CREATE TABLE project (\n" +
                    "\tid int8 NOT NULL GENERATED ALWAYS AS IDENTITY,\n" +
                    "\tproject_name varchar NULL,\n" +
                    "\tclient varchar NULL,\n" +
                    "\tstart_date date NULL,\n" +
                    "\tend_date date NULL,\n" +
                    "\tmethodology varchar NULL,\n" +
                    "\tproject_manager_id int8 NULL,\n" +
                    "\tteam_id int8 NULL,\n" +
                    "\tCONSTRAINT project_pk PRIMARY KEY (id),\n" +
                    "\tCONSTRAINT project_fk FOREIGN KEY (project_manager_id) REFERENCES employee(id),\n" +
                    "\tCONSTRAINT project_fk1 FOREIGN KEY (team_id) REFERENCES team(id)\n" +
                    ");\n" +
                    "\n" +
                    "\n" +
                    "-- public.team_employee definition\n" +
                    "\n" +
                    "-- Drop table\n" +
                    "\n" +
                    "-- DROP TABLE team_employee;\n" +
                    "\n" +
                    "CREATE TABLE team_employee (\n" +
                    "\tteam_id int8 NOT NULL,\n" +
                    "\temployee_id int8 NOT NULL,\n" +
                    "\tCONSTRAINT team_employee_pk PRIMARY KEY (team_id, employee_id),\n" +
                    "\tCONSTRAINT team_employee_fk FOREIGN KEY (team_id) REFERENCES team(id),\n" +
                    "\tCONSTRAINT team_employee_fk_1 FOREIGN KEY (employee_id) REFERENCES employee(id)\n" +
                    ");").execute();
        }
    }
}
