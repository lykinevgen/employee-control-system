package com.hw.employee_control_system.dao;

import com.hw.employee_control_system.entity.EmployeeEntity;
import com.hw.employee_control_system.enums.EnglishLanguageLevel;
import com.hw.employee_control_system.enums.Seniority;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.sql.Date;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

public class EmployeeDAOTests extends DAOTest {

    DAO employeeDAO = new EmployeeDAO();

    EmployeeEntity employeeEntity;

    @BeforeEach
    public void refreshFixture(){
        employeeEntity = new EmployeeEntity().builder()
                .firstName("Semen").secondName("Parfenov").thirdName("Master")
                .dateOfBirth(Date.valueOf("1998-12-10")).dateOfEmployment(Date.valueOf(LocalDate.now()))
                .seniority(Seniority.M1).englishLanguageLevel(EnglishLanguageLevel.B1)
                .email("darkFantasies@gmail.com").phoneNumber("89814563322").skype("semenParfenovSkype")
                .project_id(null).feedback_id(null).build();
    }

    @Test
    public void create() throws SQLException {
        assertTrue(employeeDAO.create(employeeEntity) > 0 );
    }

    @Test
    public void readExisting() throws SQLException {
        long createdEntityId = employeeDAO.create(employeeEntity);
        Optional<EmployeeEntity> readResult = employeeDAO.read(createdEntityId);
        assertTrue(readResult.isPresent());
    }

    @Test
    public void readNonExisting() throws SQLException {
        Optional<EmployeeEntity> read = employeeDAO.read(1L);
        assertFalse(read.isPresent());
    }

    @Test
    public void updateExisting() throws SQLException {
        long createdEntityId = employeeDAO.create(employeeEntity);
        employeeEntity.setId(createdEntityId);
        employeeEntity.setPhoneNumber("88005553535");
        assertTrue(employeeDAO.update(employeeEntity));
        Optional<EmployeeEntity> editedEmployeeEntity = employeeDAO.read(createdEntityId);
        assertTrue(editedEmployeeEntity.isPresent());
        assertEquals(employeeEntity, editedEmployeeEntity.get());
    }

    @Test
    public void updateNonExisting() throws SQLException {
        employeeEntity.setId(100L);
        employeeEntity.setPhoneNumber("88005553535");
        assertFalse(employeeDAO.update(employeeEntity));

    }

    @Test
    public void delete() throws SQLException {
        Long createdEntityId = employeeDAO.create(employeeEntity);
        assertTrue(employeeDAO.delete(createdEntityId));
    }

    @Test
    public void deleteNonExisting() throws SQLException {
        assertFalse(employeeDAO.delete(10L));
    }

    @Test
    public void readAll() throws SQLException {
        List<EmployeeEntity> list = new LinkedList<>();
        for (int i = 0; i < 5; i++){
            employeeEntity.setId(employeeDAO.create(employeeEntity));
            list.add(employeeEntity);
        }
        Optional<List<EmployeeEntity>> employeeEntities = employeeDAO.readAll();
        assertTrue(employeeEntities.isPresent());
        List<EmployeeEntity> readAllList = employeeEntities.get();
        assertTrue(readAllList.containsAll(list));
    }
}
