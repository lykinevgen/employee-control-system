package com.hw.employee_control_system.dao;

import com.hw.employee_control_system.entity.FeedbackEntity;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.sql.Date;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

public class FeedbackDAOTests extends DAOTest {
    DAO feedbackDAO = new FeedbackDAO();

    FeedbackEntity feedbackEntity;
    
    @BeforeEach
    public void refreshFixture(){
        feedbackEntity = new FeedbackEntity().builder()
                .description("Очень плохая музыка!")
                .date(Date.valueOf("2018-09-09")).build();
    }

    @Test
    public void create() throws SQLException {
        assertTrue(feedbackDAO.create(feedbackEntity) > 0);
    }

    @Test
    public void readExisting() throws SQLException {
        long createdTeamId = feedbackDAO.create(feedbackEntity);
        assertTrue(createdTeamId > 0);
        assertTrue(feedbackDAO.read(createdTeamId).isPresent());

    }

    @Test
    public void readNonExisting() throws SQLException {
        Optional<FeedbackEntity> read = feedbackDAO.read(1L);
        assertFalse(read.isPresent());
    }

    @Test
    public void updateExisting() throws SQLException {
        long createdEntityId = feedbackDAO.create(feedbackEntity);
        feedbackEntity.setId(createdEntityId);
        feedbackEntity.setDescription("Всё по аксиоме Эскобара, б***ь.");
        assertTrue(feedbackDAO.update(feedbackEntity));
        Optional<FeedbackEntity> editedFeedbackEntity = feedbackDAO.read(createdEntityId);
        assertTrue(editedFeedbackEntity.isPresent());
        assertEquals(feedbackEntity, editedFeedbackEntity.get());
    }

    @Test
    public void updateNonExistent() throws SQLException {
        feedbackEntity.setId(100L);
        assertFalse(feedbackDAO.update(feedbackEntity));

    }

    @Test
    public void delete() throws SQLException {
        Long createdEntityId = feedbackDAO.create(feedbackEntity);
        assertTrue(feedbackDAO.delete(createdEntityId));
    }

    @Test
    public void deleteNonExistent() throws SQLException {
        assertFalse(feedbackDAO.delete(10L));
    }

    @Test
    public void readAll() throws SQLException {
        List<FeedbackEntity> list = new LinkedList<>();
        for (int i = 0; i < 5; i++){
            feedbackEntity.setId(feedbackDAO.create(feedbackEntity));
            list.add(feedbackEntity);
        }
        Optional<List<FeedbackEntity>> feedbackEntities = feedbackDAO.readAll();
        assertTrue(feedbackEntities.isPresent());
        List<FeedbackEntity> readAllList = feedbackEntities.get();
        assertTrue(readAllList.containsAll(list));
    }
}
