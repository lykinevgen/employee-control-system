package com.hw.employee_control_system.dao;

import com.hw.employee_control_system.entity.ProjectEntity;
import com.hw.employee_control_system.enums.Methodology;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.sql.Date;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

public class ProjectDAOTests extends DAOTest {
    DAO projectDAO = new ProjectDAO();

    ProjectEntity projectEntity;

    @BeforeEach
    public void refreshFixture(){
        projectEntity = new ProjectEntity().builder()
                .projectName("GREAT PROJECT")
                .client("Mityay")
                .methodology(Methodology.AGILE)
                //.start(Date.valueOf("2015-09-15"))
                .end(Date.valueOf("2020-09-15"))
                .build();
    }

    @Test
    public void create() throws SQLException {
        assertTrue(projectDAO.create(projectEntity) > 0 );
    }

    @Test
    public void readExisting() throws SQLException {
        long createdEntityId = projectDAO.create(projectEntity);
        Optional<ProjectEntity> readResult = projectDAO.read(createdEntityId);
        assertTrue(readResult.isPresent());
    }

    @Test
    public void readNonExisting() throws SQLException {
        Optional<ProjectEntity> read = projectDAO.read(1L);
        assertFalse(read.isPresent());
    }

    @Test
    public void updateExisting() throws SQLException {
        long createdEntityId = projectDAO.create(projectEntity);
        projectEntity.setId(createdEntityId);
        projectEntity.setClient("Volodya");
        assertTrue(projectDAO.update(projectEntity));
        Optional<ProjectEntity> editedProjectEntity = projectDAO.read(createdEntityId);
        assertTrue(editedProjectEntity.isPresent());
        assertEquals(projectEntity, editedProjectEntity.get());
    }

    @Test
    public void updateNonExistent() throws SQLException {
        projectEntity.setId(100L);
        projectEntity.setClient("Volodya");
        assertFalse(projectDAO.update(projectEntity));

    }

    @Test
    public void delete() throws SQLException {
        Long createdEntityId = projectDAO.create(projectEntity);
        assertTrue(projectDAO.delete(createdEntityId));
    }

    @Test
    public void deleteNonExistent() throws SQLException {
        assertFalse(projectDAO.delete(10L));
    }

    @Test
    public void readAll() throws SQLException {
        List<ProjectEntity> list = new LinkedList<>();
        for (int i = 0; i < 5; i++){
            projectEntity.setId(projectDAO.create(projectEntity));
            list.add(projectEntity);
        }
        Optional<List<ProjectEntity>> projectEntities = projectDAO.readAll();
        assertTrue(projectEntities.isPresent());
        List<ProjectEntity> readAllList = projectEntities.get();
        assertTrue(readAllList.containsAll(list));
    }

}
