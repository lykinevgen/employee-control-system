package com.hw.employee_control_system.dao;

import com.hw.employee_control_system.entity.TeamEntity;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

public class TeamDAOTests extends DAOTest {

    DAO teamDAO = new TeamDAO();

    TeamEntity teamEntity;
    @BeforeEach
    public void refreshFixture(){
        teamEntity = new TeamEntity().builder()
                .teamName("Team A").build();
    }

    @Test
    public void create() throws SQLException {
        assertTrue(teamDAO.create(teamEntity) > 0);
    }

    @Test
    public void readExisting() throws SQLException {
        long createdTeamId = teamDAO.create(teamEntity);
        assertTrue(createdTeamId > 0);
        assertTrue(teamDAO.read(createdTeamId).isPresent());

    }

    @Test
    public void readNonExisting() throws SQLException {
        Optional<TeamEntity> read = teamDAO.read(1L);
        assertFalse(read.isPresent());
    }

    @Test
    public void updateExisting() throws SQLException {
        long createdEntityId = teamDAO.create(teamEntity);
        teamEntity.setId(createdEntityId);
        teamEntity.setTeamName("Yellow Team");
        assertTrue(teamDAO.update(teamEntity));
        Optional<TeamEntity> editedTeamEntity = teamDAO.read(createdEntityId);
        assertTrue(editedTeamEntity.isPresent());
        assertEquals(teamEntity, editedTeamEntity.get());
    }

    @Test
    public void updateNonExistent() throws SQLException {
        teamEntity.setId(100L);
        assertFalse(teamDAO.update(teamEntity));

    }

    @Test
    public void delete() throws SQLException {
        Long createdEntityId = teamDAO.create(teamEntity);
        assertTrue(teamDAO.delete(createdEntityId));
    }

    @Test
    public void deleteNonExistent() throws SQLException {
        assertFalse(teamDAO.delete(10L));
    }

    @Test
    public void readAll() throws SQLException {
        List<TeamEntity> list = new LinkedList<>();
        for (int i = 0; i < 5; i++){
            teamEntity.setTeamName(Integer.toString(i));
            teamEntity.setId(teamDAO.create(teamEntity));
            list.add(teamEntity);
        }
        Optional<List<TeamEntity>> teamEntities = teamDAO.readAll();
        assertTrue(teamEntities.isPresent());
        List<TeamEntity> readAllList = teamEntities.get();
        assertTrue(readAllList.containsAll(list));
    }
    
}
