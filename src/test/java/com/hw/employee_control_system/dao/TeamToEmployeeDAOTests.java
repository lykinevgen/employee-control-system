package com.hw.employee_control_system.dao;

import com.hw.employee_control_system.entity.EmployeeEntity;
import com.hw.employee_control_system.entity.TeamEntity;
import com.hw.employee_control_system.entity.TeamToEmployeeEntity;
import com.hw.employee_control_system.enums.EnglishLanguageLevel;
import com.hw.employee_control_system.enums.Seniority;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.sql.Date;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

public class TeamToEmployeeDAOTests extends DAOTest{
    TeamToEmployeeDAO teamToEmployeeDAO = new TeamToEmployeeDAO();
    TeamDAO teamDAO = new TeamDAO();
    EmployeeDAO employeeDAO = new EmployeeDAO();

    EmployeeEntity employeeEntity;
    TeamEntity teamEntity;
    TeamToEmployeeEntity teamToEmployeeEntity;

    @BeforeEach
    public void refreshFixture() throws SQLException {
        employeeEntity = new EmployeeEntity().builder()
                .firstName("Semen").secondName("Parfenov").thirdName("Master")
                .dateOfBirth(Date.valueOf("1998-12-10")).dateOfEmployment(Date.valueOf(LocalDate.now()))
                .seniority(Seniority.M1).englishLanguageLevel(EnglishLanguageLevel.B1)
                .email("darkFantasies@gmail.com").phoneNumber("89814563322").skype("semenParfenovSkype")
                .project_id(null).feedback_id(null).build();
        teamEntity = new TeamEntity().builder()
                .teamName("Team A").build();
        teamToEmployeeEntity = new TeamToEmployeeEntity().builder()
                .build();

        Long teamId = teamDAO.create(teamEntity);
        Long employeeId = employeeDAO.create(employeeEntity);
        teamToEmployeeEntity.setEmployeeId(employeeId);
        teamToEmployeeEntity.setTeamId(teamId);
    }

    @Test
    public void create() throws SQLException {
        assertTrue(teamToEmployeeDAO.create(teamToEmployeeEntity) > 0);
    }

    @Test
    public void readExisting() throws SQLException {
        teamToEmployeeDAO.create(teamToEmployeeEntity);
        assertTrue(teamToEmployeeDAO.read(teamToEmployeeEntity).isPresent());
    }

    @Test
    public void readNonExisting() throws SQLException {
        teamToEmployeeEntity.setEmployeeId(100L);
        teamToEmployeeEntity.setTeamId(100L);
        Optional<TeamToEmployeeEntity> read = teamToEmployeeDAO.read(teamToEmployeeEntity);
        assertFalse(read.isPresent());
    }

    @Test
    public void updateExisting() throws SQLException {
        teamToEmployeeDAO.create(teamToEmployeeEntity);
        teamEntity.setTeamName("Команда Ы");
        Long createdTeamId = teamDAO.create(teamEntity);

        TeamToEmployeeEntity newTeamToEmployeeEntity = new TeamToEmployeeEntity(createdTeamId, teamToEmployeeEntity.getEmployeeId());

        assertTrue(teamToEmployeeDAO.update(teamToEmployeeEntity, newTeamToEmployeeEntity));
        Optional<TeamToEmployeeEntity> editedEntity = teamToEmployeeDAO.read(newTeamToEmployeeEntity);
        assertTrue(editedEntity.isPresent());
        assertEquals(newTeamToEmployeeEntity, editedEntity.get());
    }

    @Test
    public void updateNonExistent() throws SQLException {
        teamToEmployeeEntity.setEmployeeId(100L);
        teamToEmployeeEntity.setTeamId(100L);
        TeamToEmployeeEntity newTeamToEmployeeEntity = new TeamToEmployeeEntity(200L, 100L);
        assertFalse(teamToEmployeeDAO.update(teamToEmployeeEntity, newTeamToEmployeeEntity));

    }

    @Test
    public void delete() throws SQLException {
        teamToEmployeeDAO.create(teamToEmployeeEntity);
        assertTrue(teamToEmployeeDAO.delete(teamToEmployeeEntity));
    }

    @Test
    public void deleteNonExistent() throws SQLException {
        assertFalse(teamToEmployeeDAO.delete(teamToEmployeeEntity));
    }

    @Test
    public void readAll() throws SQLException {
        List<TeamToEmployeeEntity> list = new LinkedList<>();
        for (int i = 0; i < 5; i++){
            teamEntity.setTeamName(Integer.toString(i));
            Long createdTeamId = teamDAO.create(teamEntity);
            teamToEmployeeEntity.setTeamId(createdTeamId);
            teamToEmployeeDAO.create(teamToEmployeeEntity);
            list.add(teamToEmployeeEntity);
        }
        Optional<List<TeamToEmployeeEntity>> projectEntities = teamToEmployeeDAO.readAll();
        assertTrue(projectEntities.isPresent());
        List<TeamToEmployeeEntity> readAllList = projectEntities.get();
        assertTrue(readAllList.containsAll(list));
    }
}
